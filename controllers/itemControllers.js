const Item = require("../models/Item");
const bcrypt = require('bcrypt');
const auth = require("../auth")

module.exports.addItem = (reqBody) => {

	let newItem = new Item({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		quantity: reqBody.quantity
	})

	return newItem.save().then((user, error) => {	
		if(error) {
			return false;
		} else {
			return true
		}
	})
}

module.exports.getAllItems = () => {
	return Item.find({ isActive: true },{name: 1, description: 1, price: 1, quantity: 1, addedOn: 1}).then(result => {	
		return result;
	})
}

module.exports.getItem = (reqParams) => {
	return Item.findById(reqParams, {name: 1, description: 1, price: 1, quantity: 1, addedOn: 1}).then(result => {
		return result;
	})
}


module.exports.updateItem = (itemId, reqBody) => {
	let updatedItem = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		quantity: reqBody.quantity
	};

	return Item.findByIdAndUpdate(itemId, updatedItem).then((item, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	})
}

module.exports.archiveItem = (itemId, reqBody) => {
	let archiveItem = {
		isActive: reqBody.isActive
	};

	return Item.findByIdAndUpdate(itemId, archiveItem).then((item, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	})
}






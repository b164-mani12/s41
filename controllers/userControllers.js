const User = require("../models/User");
const Item = require("../models/Item");
const bcrypt = require('bcrypt');
const auth = require("../auth");

module.exports.checkUsernameExists = (reqBody) => {
	return User.find({ username: reqBody.username }).then(result => {
		if(result.length > 0 ){
			return true;
		}else{
			return false;
		}
	})
}

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		username: reqBody.username,
		mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {	
		if(error) {
			return false;
		} else {
			return true
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({ username: reqBody.username }).then(result => {
		if(result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			
			if(isPasswordCorrect){
				return { accessToken : auth.createAccessToken(result.toObject())}
			} else {
				return false;
			}
		};
	});
};

module.exports.updateUserType = (userId, reqBody) => {
	let updatedUserType = {
		isAdmin: reqBody.isAdmin
	};

	return User.findByIdAndUpdate(userId, updatedUserType).then((userId, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	})
}

// module.exports.createOrder = async (data) => {
// 		let itemArrayCustomers = [];
// 		for (i = 0; i < data.orderItems.length; i++){
// 			//console.log(data.orderItems.length);

// 			let isUserUpdated = await User.findById(data.userId).then( user => {
// 				//console.log(user);

// 				user.orderItems.push({ itemId: data.orderItems[i].itemId});
						
// 				return user.save().then((user, err)=> { return (err) ? false : true })
				
// 			});


// 			let isItemUpdated = await Item.findById(data.orderItems[i].itemId).then(item => {
// 				//console.log(item);
			
// 				if(data.orderItems[i].quantity <= item.quantity ){

// 						itemArrayCustomers.push({itemId: item.id})
// 						itemArrayCustomers.push({ userId: data.userId });
// 						//console.log(data.orderItems.quantity);
// 						item.quantity -= data.orderItems[i].quantity
// 						itemArrayCustomers.push({quantity: item.quantity})
// 						//console.log(itemArrayCustomers)
// 					//console.log(itemArrayCustomers);
// 					if(i === data.orderItems.length -1){
// 						//console.log(itemArrayCustomers);
// 						for (x = 0; x < itemArrayCustomers.length; x++){
							
// 							item.customers.push({userId: itemArrayCustomers[x].userId});
// 							item.quantity = itemArrayCustomers[x].quantity;
// 							item.id = itemArrayCustomers[x].id

// 							return item.save().then((item, err)=> { return (err) ? false : true })
// 						}
// 					}

// 				}
// 			});

			
// 			if(isUserUpdated && isItemUpdated){
				
// 			}else {
// 				//return false;
// 			}	
// 		} 
// 		//console.log(i)
// 		if(i === data.orderItems.length){
// 			return true;
// 		}
			
// }

module.exports.createOrder = async (data) => {

		for (i = 0; i < data.orderItems.length; i++){
			//console.log(data.orderItems.length);

			let isUserUpdated = await User.findById(data.userId).then( user => {
				//console.log(user);

				user.orderItems.push({ itemId: data.orderItems[i].itemId});
						
				return user.save().then((user, err)=> { return (err) ? false : true })
				
			});

			
			let isItemUpdated = await Item.findById(data.orderItems[i].itemId).then(item => {
				//console.log(item);

				if(data.orderItems[i].quantity <= item.quantity ){

						item.customers.push({ userId: data.userId });
						//console.log(data.orderItems.quantity);
						item.quantity -= data.orderItems[i].quantity


						return item.save().then((item, err)=> { return (err) ? false : true })
				}
			});

			
			if(isUserUpdated && isItemUpdated){
				
			}else {
				return false;
			}	
		} 
		if(i === data.orderItems.length){
			return true;
		}
			
}


module.exports.getOrders = (data) => {
		return User.findById(data.userId, {username:1, orderItems: 1}).then(result => {
			return result;
	});
}

module.exports.getAllOrders = () => {
		return User.find({orderItems: {$exists : true, $not: {$size: 0}}}, {username: 1, orderItems: 1}).then(result => {
				return result;	
	});
}

module.exports.getAllUsers = () => {
	return User.find({}).then(result => {	
		return result;
	})
}

const express = require("express");
const router = express.Router();
const UserController = require("../controllers/userControllers")
const auth = require("../auth");

router.post("/checkUsername", (req, res) => {
	UserController.checkUsernameExists(req.body).then(result => res.send(result))
});


router.post("/register", (req, res) => {
	UserController.registerUser(req.body).then(result => res.send(result));
});


router.post("/login", (req, res) => {
	UserController.loginUser(req.body).then(result => res.send(result));
});


router.put("/usertype/:userId",auth.verify, (req,res) => {
	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		UserController.updateUserType(req.params.userId,req.body).then(result => res.send(result));
	} else {
		res.send(false)
	}
} )

router.post("/createorder", auth.verify, (req, res) => {

	let data = {
		userId : auth.decode(req.headers.authorization).id,
		orderItems : req.body
	}
	
	UserController.createOrder(data).then(result => res.send(result));
})


router.put("/usertype/:userId",auth.verify, (req,res) => {
	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		UserController.updateUserType(req.params.userId,req.body).then(result => res.send(result));
	} else {
		res.send(false)
	}
} )

router.get("/userOrders", (req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id
	}

	UserController.getOrders(data).then(result => res.send(result))

})


router.get("/allOrders",auth.verify, (req,res) => {
	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		UserController.getAllOrders().then(result => res.send(result));
		
	} else {
		res.send(false)
	}
})

router.get("/allUsers",auth.verify, (req,res) => {
	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		UserController.getAllUsers().then(result => res.send(result));
		
	} else {
		res.send(false)
	}
})


module.exports = router;
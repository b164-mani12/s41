const mongoose = require("mongoose");

const itemSchema = new mongoose.Schema({
	name: {
		type : String,
		required: [true, "Course is required"]
	},

	description: {
		type: String,
		required :[true, "Description is required"]
	},

	price: {
		type: Number,
		required: [true, "Price is required"]
	},

	isActive: {
		type : Boolean,
		default: true
	},

	quantity: {
		type : Number,
		default: 0
	},

	addedOn: {
		type: Date,
		default: new Date()
	},

	customers: [
		{
			userId:{
				type: String,
				required: [true, "UserId is required"]
			},

			boughtOn: {
				type: Date,
				default: new Date()
			}
		}
	]
})


module.exports = mongoose.model("Item", itemSchema)